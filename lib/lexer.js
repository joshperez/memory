(function () {
function last($$list) {
    var $$offset = 0, _ = $$list.slice(0, $$offset = $$list.length - 1), x = $$list[$$offset];
    return x;
}
function filter(fn, list) {
    return Array.prototype.filter.call(list, function (node, index, list) {
        return fn(node, index + 1, list);
    });
}
function partial(f) {
    var args = Array.prototype.slice.call(arguments, 1);
    return function () {
        var args_ = Array.prototype.slice.call(arguments, 0);
        return f.apply(f, args.concat(args_));
    };
}
function length(of) {
    return of.length;
}
var rx, identifiers, ignore, getType, extract, parseAlphanumeric, filterIdentifiersFn, returnIdentifier, parseIdentifier, parseRegularExpressions, getToken, processChunk, getLength, incrLine, returnTokens, tokenize;
rx = {
    IDENTIFIER: /^([a-zA-Z_$][0-9a-zA-Z_\-]*)/,
    NUMBER: /^((\d+\.)?\d+)+/,
    STRING: /^'(.*?)'/,
    LAMBDA: /^->/,
    OPERATION: /^(<\+|<=|>=|>|<|=>|(!|=)==?|\|\||\&\&|!!|\+\+|\+:)+/,
    TERMINATOR: /^\n/,
    REGEX: /^(\/((?![\s=])[^[\/\n\\]*(?:(?:\\[\s\S]|\[[^\]\n\\]*(?:\\[\s\S][^\]\n\\]*)*])[^[\/\n\\]*)*)\/)([imgy]{0,4})(?!\w)/,
    COMMENTS: /^(#.*)/,
    WHITESPACE: /^ /
};
identifiers = {
    KEYWORDS: [
        'arguments',
        'construct',
        'if',
        'then',
        'else',
        'import',
        'export',
        'try',
        'catch',
        'none'
    ],
    COMPARE: [
        '===',
        '!==',
        '==',
        '!=',
        '>',
        '>=',
        '<',
        '<=',
        'is',
        'isnt'
    ],
    LEFT_OPERATORS: [
        '++',
        '!!',
        '<+'
    ],
    RIGHT_OPERATORS: [
        '=>',
        '+:'
    ],
    NOT: [
        'not'
    ],
    LOGIC: [
        'and',
        'or',
        '&&',
        '||'
    ],
    BOOL: [
        'true',
        'false'
    ]
};
ignore = [
    'COMMENTS',
    'WHITESPACE'
];
getType = function getType(a) {
    return last(filter(function (x) {
        return rx[x].test(a);
    }, Object.keys(rx)));
};
extract = function extract(key, chunk) {
    return rx[key].exec(chunk);
};
parseAlphanumeric = function parseAlphanumeric(t, chunk) {
    return extract(t, chunk)[1];
};
filterIdentifiersFn = function filterIdentifiersFn(iden) {
    return partial(filter, function (x) {
        return identifiers[x].indexOf(iden) >= 0;
    });
};
returnIdentifier = function returnIdentifier(item, type) {
    switch (false) {
    case !(type === 'KEYWORDS'):
        return [
            item.toUpperCase(),
            item
        ];
    default:
        return [
            type,
            item
        ];
    }
};
parseIdentifier = function parseIdentifier(t, chunk) {
    var item, filterIdentifiers, token;
    item = extract(t, chunk);
    filterIdentifiers = filterIdentifiersFn(item[1]);
    token = last(filterIdentifiers(Object.keys(identifiers)));
    return returnIdentifier(item[1], token || t);
};
parseRegularExpressions = function parseRegularExpressions(t, chunk) {
    var item, regexp;
    item = extract(t, chunk);
    regexp = new RegExp(item[2], item[3]);
    return [
        'REGEXP',
        regexp
    ];
};
getToken = function getToken(chunk, n, t) {
    switch (false) {
    case !(t === 'IDENTIFIER'):
        return parseIdentifier(t, chunk);
    case !(t === 'NUMBER'):
        return parseAlphanumeric(t, chunk);
    case !(t === 'STRING'):
        return parseAlphanumeric(t, chunk);
    case !(t === 'OPERATION'):
        return parseIdentifier(t, chunk);
    case !(t === 'LAMBDA'):
        return '->';
    case !(t === 'REGEX'):
        return parseRegularExpressions(t, chunk);
    case !(t === 'COMMENTS'):
        return parseAlphanumeric(t, chunk);
    case !(t === 'WHITESPACE'):
        return ' ';
    case !(t === 'TERMINATOR'):
        return ' ';
    default:
        return [
            chunk[0],
            chunk[0]
        ];
    }
};
processChunk = function processChunk(code, n) {
    var type, token;
    type = getType(code);
    token = getToken(code, n, type);
    if (Array.isArray(token)) {
        token.push(n.line);
        return token;
    } else {
        return [
            type,
            token,
            n.line
        ];
    }
};
getLength = function getLength(i, token) {
    switch (false) {
    case !(token === 'STRING'):
        return i + 2;
    default:
        return i;
    }
};
incrLine = function incrLine(line, type) {
    switch (false) {
    case !(type === 'TERMINATOR'):
        return line + 1;
    default:
        return line;
    }
};
returnTokens = function returnTokens(code, n) {
    var tokens, len, chunk, token, n2;
    tokens = [];
    len = length(code);
    chunk = code.substr(n.index, len);
    if (chunk) {
        token = processChunk(chunk, n);
        tokens.push(token);
        n2 = {
            index: getLength(n.index + token[1].toString().length, token[0]),
            line: incrLine(n.line, token[0])
        };
        return tokens.concat(returnTokens(code, n2));
    } else {
        return tokens;
    }
};
this.tokenize = tokenize = function tokenize(code) {
    return filter(function (token) {
        return 0 > ignore.indexOf(token[0]);
    }, returnTokens(code, {
        index: 0,
        line: 0
    }));
};
}.call(typeof module !== "undefined" ? module.exports : this))